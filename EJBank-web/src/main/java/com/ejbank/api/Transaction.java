package com.ejbank.api;

import com.ejbank.pojo.*;
import com.ejbank.transaction.TransactionBeanLocal;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * The type Transaction.
 */
@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class Transaction {

    @EJB
    private TransactionBeanLocal transactionBeanLocal;

    /**
     * Gets transaction.
     *
     * @param accountId the account id
     * @param offset    the offset
     * @param userId    the user id
     * @return the transaction
     */
    @GET
    @Path("list/{account_id}/{offset}/{user_id}")
    public PojoListTransaction getTransaction(@PathParam("account_id") Integer accountId, @PathParam("offset") Integer offset, @PathParam("user_id") Integer userId) {
        return transactionBeanLocal.getTransactions(accountId, offset, userId);
    }

    /**
     * Gets preview.
     *
     * @param pojoPreviewRequest the pojo preview request
     * @return the preview
     */
    @POST
    @Path("preview/")
    public PojoPreviewResponse getPreview(PojoPreviewRequest pojoPreviewRequest) {
        return transactionBeanLocal.getPreview(pojoPreviewRequest);
    }

    /**
     * Gets apply.
     *
     * @param pojoApplyRequest the pojo apply request
     * @return the apply
     */
    @POST
    @Path("apply/")
    public PojoApplyResponse getApply(PojoApplyRequest pojoApplyRequest) {
        return transactionBeanLocal.getApply(pojoApplyRequest);
    }

    /**
     * Gets list aprouvement.
     *
     * @param pojoTransactionApprouvementRequest the pojo transaction approuvement request
     * @return the list aprouvement
     */
    @POST
    @Path("validation/")
    public PojoTransactionApprouvementResponse getListAprouvement(PojoTransactionApprouvementRequest pojoTransactionApprouvementRequest) {
        return transactionBeanLocal.setApprouvement(pojoTransactionApprouvementRequest);
    }

    /**
     * Gets notification.
     *
     * @param userId the user id
     * @return the notification
     */
    @GET
    @Path("validation/notification/{user_id}")
    public int getNotification(@PathParam("user_id") Integer userId){
        return transactionBeanLocal.getNotification(userId);
    }

}



