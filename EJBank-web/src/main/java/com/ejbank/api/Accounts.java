package com.ejbank.api;

import com.ejbank.account.AccountsBean;
import com.ejbank.pojo.PojoListAccount;
import com.ejbank.pojo.PojoListAccountAttached;
import com.ejbank.pojo.PojoListAccountForAdvisor;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * The type Accounts.
 */
@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class Accounts {
    @EJB
    private AccountsBean accountsBean;

    /**
     * Gets user.
     *
     * @param id the id
     * @return the user
     */
    @GET
    @Path("/{user_id}")
    public PojoListAccount getUser(@PathParam("user_id") Integer id) {
        return accountsBean.getAccountInfo(id);
    }

    /**
     * Gets attached.
     *
     * @param id the id
     * @return the attached
     */
    @GET
    @Path("attached/{user_id}")
    public PojoListAccountAttached getAttached(@PathParam("user_id") Integer id) {
        return accountsBean.getAccountInfoAttached(id);
    }

    /**
     * Gets all account.
     *
     * @param id the id
     * @return the all account
     */
    @GET
    @Path("all/{user_id}")
    public PojoListAccountForAdvisor getAllAccount(@PathParam("user_id") Integer id) {
        return accountsBean.getAllAccount(id);
    }
}
