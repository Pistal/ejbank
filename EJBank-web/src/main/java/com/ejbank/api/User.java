package com.ejbank.api;

import com.ejbank.pojo.PojoUser;
import com.ejbank.user.UserBeanLocal;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * The type User.
 */
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class User {

    @EJB
    private UserBeanLocal userBeanLocal;

    /**
     * Gets user.
     *
     * @param id the id
     * @return the user
     */
    @GET
    @Path("/{user_id}")
    public PojoUser getUser(@PathParam("user_id") Integer id) {
        return userBeanLocal.getUserInfo(id);
    }
}


