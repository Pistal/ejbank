package com.ejbank.api;

import com.ejbank.account.AccountBean;
import com.ejbank.account.AccountsBean;
import com.ejbank.pojo.PojoAccountDetail;
import com.ejbank.pojo.PojoListAccount;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * The type Account.
 */
@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class Account {

    @EJB
    private AccountBean accountBean;

    /**
     * Gets user.
     *
     * @param idUser    the id user
     * @param idAccount the id account
     * @return the user
     */
    @GET
    @Path("/{account_id}/{user_id}")
    public PojoAccountDetail getUser(@PathParam("user_id") Integer idUser, @PathParam("account_id") Integer idAccount) {
        return accountBean.getDetail(idUser, idAccount);
    }
}
