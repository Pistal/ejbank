package com.ejbank.account;

import com.ejbank.pojo.PojoListAccount;
import com.ejbank.pojo.PojoListAccountAttached;
import com.ejbank.pojo.PojoListAccountForAdvisor;

import javax.ejb.Local;

/**
 * The interface Accounts bean local.
 */
@Local
public interface AccountsBeanLocal {
    /**
     * Gets account info.
     *
     * @param id the id
     * @return the account info
     */
    PojoListAccount getAccountInfo(int id);

    /**
     * Gets account info attached.
     *
     * @param id the id
     * @return the account info attached
     */
    PojoListAccountAttached getAccountInfoAttached(int id);

    /**
     * Gets all account.
     *
     * @param id the id
     * @return the all account
     */
    PojoListAccountForAdvisor getAllAccount(int id);
}
