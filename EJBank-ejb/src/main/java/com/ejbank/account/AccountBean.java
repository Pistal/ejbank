package com.ejbank.account;

import com.ejbank.model.*;
import com.ejbank.pojo.PojoAccountDetail;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The type Account bean.
 */
@Startup
@Stateless
@LocalBean
public class AccountBean implements AccountBeanLocal {
    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;


    @Override
    public PojoAccountDetail getDetail(int idUser, int idAccount) {
        Account account = em.find(Account.class, idAccount);
        if (account.getCustomer().getAdvisor().getId() == idUser || account.getCustomer().getId() == idUser) {
            return new PojoAccountDetail(account.getId(), account.getCustomer().getFirstName(), account.getBalance(), account.getCustomer().getAdvisor().getFirstName(), account.getType().getRate(), getInterest(idAccount, true));
        } else {
            throw new IllegalStateException("you are not authorized to access this account");
        }
    }


    private double getInterest(int idAccount, boolean isCurrentDate) {
        Account account = em.find(Account.class, idAccount);
        if (account.getType().getRate() == 0) {
            return 0;
        }
        Calendar limit = Calendar.getInstance();
        int year;
        if (isCurrentDate) {
            year = limit.get(Calendar.YEAR);
        } else {
            limit.set(Calendar.YEAR, limit.get(Calendar.YEAR) - 1);
            year = limit.get(Calendar.YEAR);
            limit.set(Calendar.MONTH, Calendar.DECEMBER);
            limit.set(Calendar.DATE, 31);
        }
        double total = 0;
        for (int i = 0; i <= limit.get(Calendar.MONTH); i++) {
            Calendar init = Calendar.getInstance();
            init.set(year, Calendar.JANUARY, 1);
            init.set(Calendar.MONTH, i);
            Calendar last = Calendar.getInstance();
            last.set(year, Calendar.JANUARY, 1);
            last.set(Calendar.MONTH, i);
            last.set(Calendar.DATE, init.getActualMaximum(Calendar.DAY_OF_MONTH));
            Double sum = (Double) em.createQuery("SELECT SUM(t.amount) from Transaction t  WHERE t.accountTo= :accountTo and t.date between :dateinit and :datefinal")
                    .setParameter("accountTo", account).setParameter("dateinit", init.getTime()).setParameter("datefinal", last.getTime()).getSingleResult();
            if (sum == null) {
                sum = (double) 0;
            }
            total += sum * (account.getType().getRate().doubleValue() / 100);
        }
        return total;
    }

    @Schedule(month = "Jan", dayOfMonth = "1", hour = "8", persistent = false)
    private void updateInterestForYear() {
        List<Account> accounts = em.createQuery("select a from Account a", Account.class).getResultList();
        accounts.stream().filter(account -> account.getType().getRate() > 0).forEach(account -> {
            double interest = getInterest(account.getId(), false);
            if(interest>0){
                Transaction transaction = new Transaction();
                transaction.setAccountFrom(em.find(Account.class,1));
                transaction.setAccountTo(account);
                transaction.setAmount(interest);
                transaction.setApplied(true);
                transaction.setDate(Calendar.getInstance().getTime());
                em.persist(transaction);
            }
        });

    }
}
