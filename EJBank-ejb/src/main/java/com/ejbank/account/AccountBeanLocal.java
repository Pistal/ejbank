package com.ejbank.account;

import com.ejbank.pojo.PojoAccountDetail;

import java.util.Date;

/**
 * The interface Account bean local.
 */
public interface AccountBeanLocal {
    /**
     * Gets detail.
     *
     * @param idUser    the id user
     * @param idAccount the id account
     * @return the detail
     */
    PojoAccountDetail getDetail( int idUser, int idAccount);
}
