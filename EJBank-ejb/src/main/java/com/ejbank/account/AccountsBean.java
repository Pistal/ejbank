package com.ejbank.account;


import com.ejbank.model.Account;
import com.ejbank.model.Advisor;
import com.ejbank.model.Customer;
import com.ejbank.model.User;
import com.ejbank.pojo.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Accounts bean.
 */
@Stateless
@LocalBean
public class AccountsBean implements AccountsBeanLocal {

    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;

    @Override
    public PojoListAccount getAccountInfo(int id) {
        Customer user = em.find(Customer.class, id);
        if(user==null){
            return new PojoListAccount(List.of(), "Customer only");
        }
        List<Account> accounts = user.getAccounts();
        List<PojoAccount> collect = accounts.stream().map(account -> new PojoAccount(account.getId(), account.getType().getName(), account.getBalance())).collect(Collectors.toList());
        return new PojoListAccount(collect, "");
    }

    @Override
    public PojoListAccountAttached getAccountInfoAttached(int id) {
        Advisor advisor = em.find(Advisor.class, id);
        if(advisor==null){
            return new PojoListAccountAttached(List.of(),"Advisor only");
        }
        List<Customer> customers = advisor.getCustomers();
        List<PojoAccountAttached> accountAttacheds = new ArrayList<>();
        customers.forEach(customer -> {
            List<Account> accounts = customer.getAccounts();
            accounts.forEach(account -> {
                Long count = (Long) em.createQuery("SELECT COUNT(t) from Transaction t  WHERE t.accountFrom = :accountFrom and t.applied = false")
                        .setParameter("accountFrom", account).getSingleResult();
                accountAttacheds.add(new PojoAccountAttached(account.getId(), customer.getFirstName(), account.getType().getName(), account.getBalance(), Math.toIntExact(count)));
            });
        });
        return new PojoListAccountAttached(accountAttacheds,"");
    }

    @Override
    public PojoListAccountForAdvisor getAllAccount(int id) {
        List<PojoAccountForAdvisor> pojoAccountForAdvisors = new ArrayList<>();
        User user = em.find(User.class, id);
        if( user instanceof Advisor){
            Advisor advisor = (Advisor) user;
            advisor.getCustomers().forEach(customer -> customer.getAccounts().forEach(account -> pojoAccountForAdvisors.add(new PojoAccountForAdvisor(account.getId(), customer.getFirstName(), account.getType().getName(), account.getBalance()))));
        }else {
            Customer customer = (Customer) user;
            customer.getAccounts().forEach(account -> pojoAccountForAdvisors.add(new PojoAccountForAdvisor(account.getId(), customer.getFirstName(), account.getType().getName(), account.getBalance())));
        }
        return new PojoListAccountForAdvisor(pojoAccountForAdvisors);

    }
}
