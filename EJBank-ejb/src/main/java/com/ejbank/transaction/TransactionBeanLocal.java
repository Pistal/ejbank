package com.ejbank.transaction;

import com.ejbank.pojo.*;

import javax.ejb.Local;

/**
 * The interface Transaction bean local.
 */
@Local
public interface TransactionBeanLocal {
    /**
     * Gets transactions.
     *
     * @param accountId the account id
     * @param offset    the offset
     * @param userId    the user id
     * @return the transactions
     */
    PojoListTransaction getTransactions(int accountId, int offset, int userId);

    /**
     * Gets preview.
     *
     * @param pojoPreviewRequest the pojo preview request
     * @return the preview
     */
    PojoPreviewResponse getPreview(PojoPreviewRequest pojoPreviewRequest);

    /**
     * Gets apply.
     *
     * @param pojoApplyRequest the pojo apply request
     * @return the apply
     */
    PojoApplyResponse getApply(PojoApplyRequest pojoApplyRequest);

    /**
     * Sets approuvement.
     *
     * @param pojoTransactionApprouvementRequest the pojo transaction approuvement request
     * @return the approuvement
     */
    PojoTransactionApprouvementResponse setApprouvement(PojoTransactionApprouvementRequest pojoTransactionApprouvementRequest);

    /**
     * Gets notification.
     *
     * @param userId the user id
     * @return the notification
     */
    int getNotification(Integer userId);
}
