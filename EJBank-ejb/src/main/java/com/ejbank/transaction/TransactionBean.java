package com.ejbank.transaction;

import com.ejbank.model.*;
import com.ejbank.pojo.*;

import javax.ejb.*;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Transaction bean.
 */
@Stateless
@LocalBean
public class TransactionBean implements TransactionBeanLocal {

    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;

    @Override
    public PojoListTransaction getTransactions(int accountId, int offset, int userId) {
        User user = em.find(User.class, userId);

        Account account = em.find(Account.class, accountId);
        if (account.getCustomer().getAdvisor().getId() != userId && account.getCustomer().getId() != userId) {
            throw new IllegalStateException("you are not authorized to access this account");
        }
        List<PojoTransaction> pojoTransactions = new ArrayList<>();
        long count = (Long) em.createQuery("SELECT COUNT(t) from Transaction t  WHERE t.accountFrom = :accountFrom OR t.accountTo= :accountTo")
                .setParameter("accountFrom", account).setParameter("accountTo", account).getSingleResult();
        List<Transaction> resultList = em.createQuery("SELECT t from Transaction t  WHERE t.accountFrom = :accountFrom OR t.accountTo= :accountTo", Transaction.class)
                .setParameter("accountFrom", account).setParameter("accountTo", account).setFirstResult(offset).setMaxResults(100).getResultList();
        if (user instanceof Advisor) {
            resultList.forEach(transaction -> {
                String s;
                if (transaction.isApplied()) {
                    s = "APPLYED";
                } else {
                    s = "TO_APPROVE";
                }
                pojoTransactions.add(new PojoTransaction(transaction.getId(), transaction.getDate().toString(), transaction.getAccountFrom().getType().getName(), transaction.getAccountFrom().getCustomer().getFirstName(), transaction.getAccountTo().getType().getName(), transaction.getAccountTo().getCustomer().getFirstName(), transaction.getAmount(), transaction.getAuthor().getFirstName() + " " + transaction.getAuthor().getLastName(), transaction.getComment(), s));
            });
        } else {
            resultList.forEach(transaction -> {
                String s;
                if (transaction.isApplied()) {
                    s = "APPLYED";
                } else {
                    s = "WAITING_APPROVE";
                }
                pojoTransactions.add(new PojoTransaction(transaction.getId(), transaction.getDate().toString(), transaction.getAccountFrom().getType().getName(), transaction.getAccountFrom().getCustomer().getFirstName(), transaction.getAccountTo().getType().getName(), transaction.getAccountTo().getCustomer().getFirstName(), transaction.getAmount(), transaction.getAuthor().getFirstName() + " " + transaction.getAuthor().getLastName(), transaction.getComment(), s));
            });
        }
        return new PojoListTransaction(pojoTransactions, Math.toIntExact(count));
    }

    @Override
    public PojoPreviewResponse getPreview(PojoPreviewRequest pojoPreviewRequest) {
        String s = checkArguments(pojoPreviewRequest.getSource(), pojoPreviewRequest.getDestination(), pojoPreviewRequest.getAmount());
        Account sourceAccount = em.find(Account.class, pojoPreviewRequest.getSource());
        Account destinationAccount = em.find(Account.class, pojoPreviewRequest.getDestination());

        if (s != null) {
            if (sourceAccount == null || destinationAccount == null) {
                return new PojoPreviewResponse(false, 0.0, 0.0, "", s);
            } else {
                return new PojoPreviewResponse(false, sourceAccount.getBalance(), sourceAccount.getBalance() - pojoPreviewRequest.getAmount(), "", s);
            }
        }
        if (sourceAccount.getCustomer().getAdvisor().getId() != pojoPreviewRequest.getAuthor() && sourceAccount.getCustomer().getId() != pojoPreviewRequest.getAuthor()) {
            return new PojoPreviewResponse(false, 0.0, 0.0, "", "you are not authorized to access this account");
        }
        return new PojoPreviewResponse(true, sourceAccount.getBalance(), sourceAccount.getBalance() - pojoPreviewRequest.getAmount(), "", "");
    }

    @Override
   // @Transactional(rollbackOn = {EntityExistsException.class, IllegalArgumentException.class, TransactionRequiredException.class  })
    public PojoApplyResponse getApply(PojoApplyRequest pojoApplyRequest) {
        String s = checkArguments(pojoApplyRequest.getSource(), pojoApplyRequest.getDestination(), pojoApplyRequest.getAmount());
        Account sourceAccount = em.find(Account.class, pojoApplyRequest.getSource());
        Account destinationAccount = em.find(Account.class, pojoApplyRequest.getDestination());
        User user = em.find(User.class, pojoApplyRequest.getAuthor());
        if (s != null) {
            return new PojoApplyResponse(false, s);
        }
        Advisor advisor = em.find(Advisor.class, pojoApplyRequest.getAuthor());

        Transaction transation = new Transaction();
        transation.setAmount(pojoApplyRequest.getAmount());
        transation.setAuthor(user);
        transation.setAccountFrom(sourceAccount);
        transation.setAccountTo(destinationAccount);
        transation.setComment(pojoApplyRequest.getComment());
        transation.setDate(Date.from(Instant.now()));
        transation.setApplied(pojoApplyRequest.getAmount() <= 1000 || advisor != null);
          em.persist(transation);
        if (transation.isApplied()) {
            sourceAccount.setBalance(sourceAccount.getBalance() - pojoApplyRequest.getAmount());
            destinationAccount.setBalance(destinationAccount.getBalance() + pojoApplyRequest.getAmount());
            em.persist(sourceAccount);
            em.persist(destinationAccount);
        }
        /*

        Pour faire le roll back il faudrait être en mode statefull
        EntityTransaction entityTransaction = em.getTransaction();
        try{


            entityTransaction.begin();
            em.persist(transation);

            if (transation.isApplied()) {
                sourceAccount.setBalance(sourceAccount.getBalance() - pojoApplyRequest.getAmount());
                destinationAccount.setBalance(destinationAccount.getBalance() + pojoApplyRequest.getAmount());
                em.persist(sourceAccount);
                em.persist(destinationAccount);
                entityTransaction.commit();
            }
        }catch (Exception exception ){
            entityTransaction.rollback();
        }
        */
        return new PojoApplyResponse(true, "Operation successful ");
    }

    @Override
    public PojoTransactionApprouvementResponse setApprouvement(PojoTransactionApprouvementRequest pojoTransactionApprouvementRequest) {
        Transaction transaction = em.find(Transaction.class, pojoTransactionApprouvementRequest.getTransaction());
        if (transaction == null)
            return new PojoTransactionApprouvementResponse(false, "Transaction doesn't exist", "Transaction doesn't exist");
        if (transaction.isApplied())
            return new PojoTransactionApprouvementResponse(false, "Transaction has bean already applied", "TTransaction has bean already applied");

        Account accountto = transaction.getAccountTo();
        Account accountFrom = transaction.getAccountFrom();
        String s = checkArguments(accountFrom.getId(), accountto.getId(), transaction.getAmount());
        if (s != null) {
            return new PojoTransactionApprouvementResponse(false, s, s);
        }
        accountFrom.setBalance(accountFrom.getBalance() + transaction.getAmount());
        accountto.setBalance(accountto.getBalance() + transaction.getAmount());
        transaction.setApplied(true);
        em.persist(transaction);

        return new PojoTransactionApprouvementResponse(true, "Approve", "");
    }

    @Override
    public int getNotification(Integer userId) {
        Advisor advisor = em.find(Advisor.class, userId);
        if (advisor == null) {
            return -1 ;
            //throw new IllegalStateException("You need to be an Advisor");
        }
        long count = (Long) em.createQuery("SELECT COUNT(t) from Transaction t WHERE t.accountFrom.customer.advisor=:advisor and t.applied=false")
                .setParameter("advisor", advisor).getSingleResult();
        return (int) count;
    }

    private String checkArguments(int sourceId, int destinationId, double amount) {
        Account sourceAccount = em.find(Account.class, sourceId);
        if (sourceAccount == null) {
            return "sourceAccount does not exist";
        }
        Account destinationAccount = em.find(Account.class, destinationId);
        if (destinationAccount == null) {
            return "destinationAccount does not exist";
        }
        if (sourceAccount.getBalance() - amount < 0) {
            return "You do not have a sufficient balance";
        }
        return null;
    }
}
