package com.ejbank.pojo;

/**
 * The type Pojo apply request.
 */
public class PojoApplyRequest {
    private int source;
    private int destination;
    private int amount;
    private int author;
    private String comment;

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Gets source.
     *
     * @return the source
     */
    public int getSource() {
        return source;
    }

    /**
     * Sets source.
     *
     * @param source the source
     */
    public void setSource(int source) {
        this.source = source;
    }

    /**
     * Gets destination.
     *
     * @return the destination
     */
    public int getDestination() {
        return destination;
    }

    /**
     * Sets destination.
     *
     * @param destination the destination
     */
    public void setDestination(int destination) {
        this.destination = destination;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public int getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(int author) {
        this.author = author;
    }
}
