package com.ejbank.pojo;

/**
 * The type Pojo apply response.
 */
public class PojoApplyResponse {
    private final boolean result;
    private final String message;

    /**
     * Instantiates a new Pojo apply response.
     *
     * @param result  the result
     * @param message the message
     */
    public PojoApplyResponse(boolean result, String message) {
        this.result = result;
        this.message = message;
    }

    /**
     * Is result boolean.
     *
     * @return the boolean
     */
    public boolean isResult() {
        return result;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }
}
