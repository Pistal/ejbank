package com.ejbank.pojo;

import java.util.Collection;

/**
 * The type Pojo list account attached.
 */
public class PojoListAccountAttached {
    private final Collection<PojoAccountAttached> pojoAccountAttacheds;
    private final String error;

    /**
     * Instantiates a new Pojo list account attached.
     *
     * @param pojoAccountAttacheds the pojo account attacheds
     * @param error                the error
     */
    public PojoListAccountAttached(Collection<PojoAccountAttached> pojoAccountAttacheds, String error) {
        this.pojoAccountAttacheds = pojoAccountAttacheds;
        this.error = error;
    }

    /**
     * Gets accounts.
     *
     * @return the accounts
     */
    public Collection<PojoAccountAttached> getAccounts() {
        return pojoAccountAttacheds;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }
}
