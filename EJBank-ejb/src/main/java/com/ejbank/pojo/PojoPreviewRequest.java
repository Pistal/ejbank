package com.ejbank.pojo;

/**
 * The type Pojo preview request.
 */
public class PojoPreviewRequest {
    private int source;
    private int destination;
    private int amount;
    private int author;

    /**
     * Gets source.
     *
     * @return the source
     */
    public int getSource() {
        return source;
    }

    /**
     * Sets source.
     *
     * @param source the source
     */
    public void setSource(int source) {
        this.source = source;
    }

    /**
     * Gets destination.
     *
     * @return the destination
     */
    public int getDestination() {
        return destination;
    }

    /**
     * Sets destination.
     *
     * @param destination the destination
     */
    public void setDestination(int destination) {
        this.destination = destination;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public int getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(int author) {
        this.author = author;
    }
}
