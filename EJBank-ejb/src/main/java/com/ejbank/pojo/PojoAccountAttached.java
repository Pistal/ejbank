package com.ejbank.pojo;

/**
 * The type Pojo account attached.
 */
public class PojoAccountAttached {
    private final int id;
    private final String user;
    private final String type;
    private final double amount;
    private final int validation;

    /**
     * Instantiates a new Pojo account attached.
     *
     * @param id         the id
     * @param user       the user
     * @param type       the type
     * @param amount     the amount
     * @param validation the validation
     */
    public PojoAccountAttached(int id, String user, String type, double amount, int validation) {
        this.id = id;
        this.user = user;
        this.type = type;
        this.amount = amount;
        this.validation = validation;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Gets validation.
     *
     * @return the validation
     */
    public int getValidation() {
        return validation;
    }
}
