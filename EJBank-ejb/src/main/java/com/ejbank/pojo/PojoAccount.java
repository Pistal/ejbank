package com.ejbank.pojo;

/**
 * The type Pojo account.
 */
public class PojoAccount {
    private final int id;
    private final String name;
    private final double balance;

    /**
     * Instantiates a new Pojo account.
     *
     * @param id      the id
     * @param name    the name
     * @param balance the balance
     */
    public PojoAccount(int id, String name, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return "Label du compte ("+name+")";
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public double getAmount() {
        return balance;
    }
}
