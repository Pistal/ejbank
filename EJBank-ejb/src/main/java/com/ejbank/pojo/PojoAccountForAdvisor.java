package com.ejbank.pojo;

/**
 * The type Pojo account for advisor.
 */
public class PojoAccountForAdvisor {
    private final int id;
    private final String user;
    private final String type;
    private final double amount;

    /**
     * Instantiates a new Pojo account for advisor.
     *
     * @param id     the id
     * @param user   the user
     * @param type   the type
     * @param amount the amount
     */
    public PojoAccountForAdvisor(int id, String user, String type, double amount) {
        this.id = id;
        this.user = user;
        this.type = type;
        this.amount = amount;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }
}
