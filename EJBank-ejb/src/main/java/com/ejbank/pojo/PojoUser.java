package com.ejbank.pojo;

/**
 * The type Pojo user.
 */
public class PojoUser {
    private final String firstName;
    private final String lastName;

    /**
     * Gets firstname.
     *
     * @return the firstname
     */
    public String getFirstname() {
        return firstName;
    }

    /**
     * Gets lastname.
     *
     * @return the lastname
     */
    public String getLastname() {
        return lastName;
    }

    /**
     * Instantiates a new Pojo user.
     *
     * @param firstName the first name
     * @param lastName  the last name
     */
    public PojoUser(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

    }
}
