package com.ejbank.pojo;

/**
 * The type Pojo account detail.
 */
public class PojoAccountDetail {
    private final int id;
    private final String owner;
    private final double balance;
    private final String advisor;
    private final int rate;
    private final double interest;

    /**
     * Instantiates a new Pojo account detail.
     *
     * @param id       the id
     * @param name     the name
     * @param balance  the balance
     * @param advisor  the advisor
     * @param rate     the rate
     * @param interest the interest
     */
    public PojoAccountDetail(int id, String name, double balance, String advisor, int rate, double interest) {
        this.id = id;
        this.owner = name;
        this.balance = balance;
        this.advisor = advisor;
        this.rate = rate;
        this.interest = interest;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public double getAmount() {
        return balance;
    }

    /**
     * Gets owner.
     *
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }


    /**
     * Gets advisor.
     *
     * @return the advisor
     */
    public String getAdvisor() {
        return advisor;
    }

    /**
     * Gets rate.
     *
     * @return the rate
     */
    public int getRate() {
        return rate;
    }

    /**
     * Gets interest.
     *
     * @return the interest
     */
    public double getInterest() {
        return interest;
    }
}
