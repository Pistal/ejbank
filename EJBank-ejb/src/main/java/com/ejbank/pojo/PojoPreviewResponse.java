package com.ejbank.pojo;

/**
 * The type Pojo preview response.
 */
public class PojoPreviewResponse {
    private final boolean result;
    private final Double before;
    private final Double after;
    private final String message;
    private final String error;

    /**
     * Instantiates a new Pojo preview response.
     *
     * @param result  the result
     * @param before  the before
     * @param after   the after
     * @param message the message
     * @param error   the error
     */
    public PojoPreviewResponse(boolean result, Double before, Double after, String message, String error) {
        this.result = result;
        this.before = before;
        this.after = after;
        this.message = message;
        this.error = error;
    }

    /**
     * Is result boolean.
     *
     * @return the boolean
     */
    public boolean isResult() {
        return result;
    }

    /**
     * Gets before.
     *
     * @return the before
     */
    public Double getBefore() {
        return before;
    }

    /**
     * Gets after.
     *
     * @return the after
     */
    public Double getAfter() {
        return after;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }
}
