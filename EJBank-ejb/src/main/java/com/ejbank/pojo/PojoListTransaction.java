package com.ejbank.pojo;

import java.util.List;

/**
 * The type Pojo list transaction.
 */
public class PojoListTransaction {

    private final List<PojoTransaction> pojoTransactions;
    private final int total;
    private String error;

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Instantiates a new Pojo list transaction.
     *
     * @param pojoTransactions the pojo transactions
     * @param count            the count
     */
    public PojoListTransaction(List<PojoTransaction> pojoTransactions, int count) {
        this.pojoTransactions = pojoTransactions;
        this.total = count;
    }

    /**
     * Gets transactions.
     *
     * @return the transactions
     */
    public List<PojoTransaction> getTransactions() {
        return pojoTransactions;
    }

    /**
     * Gets total.
     *
     * @return the total
     */
    public int getTotal() {
        return total;
    }
}
