package com.ejbank.pojo;

import com.ejbank.model.Transaction;

/**
 * The type Pojo transaction approuvement request.
 */
public class PojoTransactionApprouvementRequest {
    private int  transaction ;
    private Boolean approve;
    private int author;


    /**
     * Gets transaction.
     *
     * @return the transaction
     */
    public int getTransaction() {
        return transaction;
    }

    /**
     * Sets transaction.
     *
     * @param transaction the transaction
     */
    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }

    /**
     * Gets approve.
     *
     * @return the approve
     */
    public Boolean getApprove() {
        return approve;
    }

    /**
     * Sets approve.
     *
     * @param approve the approve
     */
    public void setApprove(Boolean approve) {
        this.approve = approve;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public int getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(int author) {
        this.author = author;
    }
}
