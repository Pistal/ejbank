package com.ejbank.pojo;

import java.util.Collection;

/**
 * The type Pojo list account.
 */
public class PojoListAccount {

    private final Collection<PojoAccount>  pojoAccountList;
    private final String error ;

    /**
     * Gets accounts.
     *
     * @return the accounts
     */
    public Collection<PojoAccount> getAccounts() {
        return pojoAccountList;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Instantiates a new Pojo list account.
     *
     * @param pojoAccountList the pojo account list
     * @param error           the error
     */
    public PojoListAccount(Collection<PojoAccount> pojoAccountList, String error) {
        this.pojoAccountList = pojoAccountList;
        this.error = error;
    }

}
