package com.ejbank.pojo;

import java.util.Collection;

/**
 * The type Pojo list account for advisor.
 */
public class PojoListAccountForAdvisor {
    private final Collection<PojoAccountForAdvisor> pojoAccountForAdvisors;
    private String error;

    /**
     * Instantiates a new Pojo list account for advisor.
     *
     * @param pojoAccountForAdvisors the pojo account for advisors
     */
    public PojoListAccountForAdvisor(Collection<PojoAccountForAdvisor> pojoAccountForAdvisors) {
        this.pojoAccountForAdvisors = pojoAccountForAdvisors;
    }

    /**
     * Gets accounts.
     *
     * @return the accounts
     */
    public Collection<PojoAccountForAdvisor> getAccounts() {
        return pojoAccountForAdvisors;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }
}
