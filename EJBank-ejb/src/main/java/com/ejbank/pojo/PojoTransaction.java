package com.ejbank.pojo;

/**
 * The type Pojo transaction.
 */
public class PojoTransaction {
    private final int id;
    private final String date;
    private final String source;
    private final String destination;
    private final String destination_user;
    private final double amount;
    private final String author;
    private final String comment;
    private final String state;
    private final String source_user;

    /**
     * Instantiates a new Pojo transaction.
     *
     * @param id               the id
     * @param date             the date
     * @param source           the source
     * @param source_user      the source user
     * @param destination      the destination
     * @param destination_user the destination user
     * @param amount           the amount
     * @param author           the author
     * @param comment          the comment
     * @param state            the state
     */
    public PojoTransaction(int id, String date, String source, String source_user, String destination, String destination_user, double amount, String author, String comment, String state) {
        this.id = id;
        this.date = date;
        this.source = source;
        this.source_user = source_user;
        this.destination = destination;
        this.destination_user = destination_user;
        this.amount = amount;
        this.author = author;
        this.comment = comment;
        this.state = state;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Gets source.
     *
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * Gets destination.
     *
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Gets source user.
     *
     * @return the source user
     */
    public String getSource_user() {
        return source_user;
    }

    /**
     * Gets destination user.
     *
     * @return the destination user
     */
    public String getDestination_user() {
        return destination_user;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }
}
