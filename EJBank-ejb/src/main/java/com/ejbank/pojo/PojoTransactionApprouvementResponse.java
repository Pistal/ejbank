package com.ejbank.pojo;

/**
 * The type Pojo transaction approuvement response.
 */
public class PojoTransactionApprouvementResponse {
    private final boolean result ;
    private final String message ;
    private final String error;


    /**
     * Instantiates a new Pojo transaction approuvement response.
     *
     * @param result  the result
     * @param message the message
     * @param error   the error
     */
    public PojoTransactionApprouvementResponse(boolean result, String message, String error) {
        this.result = result;
        this.message = message;
        this.error = error;
    }

    /**
     * Is result boolean.
     *
     * @return the boolean
     */
    public boolean isResult() {
        return result;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }
}
