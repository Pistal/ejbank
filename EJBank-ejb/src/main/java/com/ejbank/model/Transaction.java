package com.ejbank.model;


import javax.persistence.*;
import java.util.Date;

/**
 * The type Transaction.
 */
@Entity
@Table(name = "ejbank_transaction")
public class Transaction {
    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "account_id_from")
    private Account accountFrom;

    @ManyToOne
    @JoinColumn(name = "account_id_to")
    private Account accountTo ;

    @ManyToOne
    @JoinColumn(name = "author")
    private User author;

    @Column(name = "amount")
    private double amount;
    @Column(name = "comment")
    private String comment;
    @Column(name = "applied")
    private boolean applied;
    @Column(name = "date")
    private Date date ;

    /**
     * Sets account from.
     *
     * @param accountFrom the account from
     */
    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    /**
     * Sets account to.
     *
     * @param accountTo the account to
     */
    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Is applied boolean.
     *
     * @return the boolean
     */
    public boolean isApplied() {
        return applied;
    }

    /**
     * Sets applied.
     *
     * @param applied the applied
     */
    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * Gets account to.
     *
     * @return the account to
     */
    public Account getAccountTo() {
        return accountTo;
    }

    /**
     * Gets account from.
     *
     * @return the account from
     */
    public Account getAccountFrom() {
        return accountFrom;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }


}
