package com.ejbank.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The type Account.
 */
@Entity
@Table(name = "ejbank_account")
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "balance", nullable = false)
    private Double balance;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_type_id")
    private AccountType type;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "accountFrom")
    private List<Transaction> transactionsFrom;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "accountTo")
    private List<Transaction> transactionsTo;

    /**
     * Gets transactions from.
     *
     * @return the transactions from
     */
    public List<Transaction> getTransactionsFrom() {
        return transactionsFrom;
    }

    /**
     * Sets transactions from.
     *
     * @param transactionsFrom the transactions from
     */
    public void setTransactionsFrom(List<Transaction> transactionsFrom) {
        this.transactionsFrom = transactionsFrom;
    }

    /**
     * Gets transactions to.
     *
     * @return the transactions to
     */
    public List<Transaction> getTransactionsTo() {
        return transactionsTo;
    }

    /**
     * Sets transactions to.
     *
     * @param transactionsTo the transactions to
     */
    public void setTransactionsTo(List<Transaction> transactionsTo) {
        this.transactionsTo = transactionsTo;
    }

    /**
     * Gets customer.
     *
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets customer.
     *
     * @param customer the customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    /**
     * Gets type.
     *
     * @return the type
     */
    public AccountType getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(AccountType type) {
        this.type = type;
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * Gets balance.
     *
     * @return the balance
     */
    public Double getBalance() {
        return balance;
    }

    /**
     * Sets balance.
     *
     * @param balance the balance
     */
    public void setBalance(Double balance) {
        this.balance = balance;
    }


}
