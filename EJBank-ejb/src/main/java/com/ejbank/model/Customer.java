package com.ejbank.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * The type Customer.
 */
@Entity
@Table(name = "ejbank_customer")
@DiscriminatorValue(value = "customer")
public class Customer extends User implements Serializable {
    //@JoinColumn(name = "id", referencedColumnName = "customer_id")
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "customer")
    private List<Account> accounts;

    @ManyToOne
    @JoinColumn(name = "advisor_id")
    private  Advisor advisor;

    /**
     * Sets advisor.
     *
     * @param advisor the advisor
     */
    public void setAdvisor(Advisor advisor) {
        this.advisor = advisor;
    }

    /**
     * Gets advisor.
     *
     * @return the advisor
     */
    public Advisor getAdvisor() {
        return advisor;
    }

    /**
     * Sets accounts.
     *
     * @param accounts the accounts
     */
    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * Gets accounts.
     *
     * @return the accounts
     */
    public List<Account> getAccounts() {
        return accounts;
    }


}
