package com.ejbank.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The type Advisor.
 */
@Entity
@Table(name = "ejbank_advisor")
@DiscriminatorValue(value = "advisor")
public class Advisor extends User implements Serializable {
    /**
     * Gets customers.
     *
     * @return the customers
     */
    public List<Customer> getCustomers() {
        return customers;
    }

    /**
     * Sets customers.
     *
     * @param customers the customers
     */
    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "advisor_id")
    private List<Customer> customers ;
}
