package com.ejbank.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The type Account type.
 */
@Entity
@Table(name = "ejbank_account_type")
public class AccountType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "name", nullable = false, length = 50)
    private String name;
    @Column(name = "rate", nullable = false)
    private Integer rate;
    @Column(name = "overdraft", nullable = false)
    private Integer overdraft;


    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Gets rate.
     *
     * @return the rate
     */
    public Integer getRate() {
        return rate;
    }

    /**
     * Sets rate.
     *
     * @param rate the rate
     */
    public void setRate(Integer rate) {
        this.rate = rate;
    }


    /**
     * Gets overdraft.
     *
     * @return the overdraft
     */
    public Integer getOverdraft() {
        return overdraft;
    }

    /**
     * Sets overdraft.
     *
     * @param overdraft the overdraft
     */
    public void setOverdraft(Integer overdraft) {
        this.overdraft = overdraft;
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

}