package com.ejbank.user;

import com.ejbank.model.User;
import com.ejbank.pojo.PojoUser;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class UserBean implements UserBeanLocal {
    @PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;

    @Override
    public PojoUser getUserInfo(int id) {
        User user = em.find(User.class, id);
        return new PojoUser(user.getFirstName(), user.getLastName());
    }
}
