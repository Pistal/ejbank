package com.ejbank.user;

import com.ejbank.pojo.PojoUser;

import javax.ejb.Local;

@Local
public interface UserBeanLocal {
    PojoUser getUserInfo(int id);
}
