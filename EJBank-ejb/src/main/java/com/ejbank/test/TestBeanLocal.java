package com.ejbank.test;

import com.ejbank.pojo.PojoUser;

import javax.ejb.Local;

@Local
public interface TestBeanLocal {
    String test();
}
