package com.ejbank.test;

import com.ejbank.model.User;
import com.ejbank.pojo.PojoUser;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class TestBean implements TestBeanLocal {
@PersistenceContext(unitName = "EJBankPU")
    private EntityManager em;
    @Override
    public String test() {
        return "Hello from EJB";
    }
}
